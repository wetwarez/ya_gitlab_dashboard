import init, { run_app } from './pkg/yew_parcel.js';
async function main() {
   await init('/pkg/yew_parcel_bg.wasm');
   run_app();
}
main()