# Yet another Gitlab dashboard

Yet another Gitlab dashboard.

Built in rust (and a little JS) using Yew, https://yew.rs/

Styling based on a theme from AdminLTE, https://adminlte.io/

This is a "Single Page App". There is no server side work other than serving assets. Retrieved data is persisted in the browser's `localstorage`.

## Why a dashboard

The Gitlab GUI is very busy and it's difficult (for me at least) to get an overview of how your project and the branches in it are doing.

Rather than a list of all the pipelines across all branches, this display aims to summarise the status of each branch in a project. Useful links to Giltab is included.

## Todo

- I used this project to learn more about rust and wasm. The code is beginner level rust and there is a lot of room for improvement.
  - There is a lot of code duplication
  - There is a lot of unnecessary usage of `clone` where a reference could have worked. (Expediency to appease the borrow checker 😄)
  - There's no tests
- Pull and display project labels.
- Improve error feedback (console to GUI)
- Input validation, strip whitespace
- Resolve `WARN:yew::agent -- The Id of the handler: 0, while present in the slab, is not associated with a callback.`
- Roll several GET API requests into fewer graphQL requests.

## Quickstart

#### Prerequisites

- Yarn
- Python3 (to serve dist folder during development)
- rust, including
  - wasm-bindgen
  - wasm-pack

### Install

```sh
yarn install
```

### Run development server

```sh
# Builds the project and opens it in a new browser tab. Auto-reloads when the project changes.
yarn start
```

### Release

You have the option of populating `.env` with the project details. This will remove the requirement to fill out the start page. (Useful for publishing a per project/group instance)

```sh
# Builds the project and places it into the `dist` folder.
yarn build
```

### Serve the built `dist` (for testing, not production)

```sh
cd dist && python ../py_serve.py
```

## Screenshots

#### Projects overview

![Projects Overview](img/projects_overview.png "Projects Overview")

#### Project branches summary

![Branches](img/projects_branches.png "Branches")

#### Commits for a branch

![Commits](img/commits.png "Commits")
