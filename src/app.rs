use super::home;
use super::start;
use crate::types::app_state::AppState;
use crate::types::general::Creds;
use crate::wasm_bindgen;
use web_sys::Element;
use yew::prelude::*;

#[wasm_bindgen(module = "/js/utils.js")]
extern "C" {
    fn re_init_tree_view();
}

pub struct App {
    link: ComponentLink<Self>,
    app_state: Box<AppState>,
}

pub enum Msg {
    StartClicked(Creds),
    AppStateUpdated(AppState),
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        App {
            app_state: Box::new(AppState::default()),
            link,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::StartClicked(new_creds) => {
                self.app_state.creds = new_creds;
                self.app_state.save_state();
                return true;
            }

            Msg::AppStateUpdated(new_app_state) => {
                self.app_state = Box::new(new_app_state);
                return true;
            }
        }
    }

    fn rendered(&mut self, _: bool) {
        re_init_tree_view();
    }

    fn view(&self) -> Html {
        let document = yew::utils::document();
        let body: Element = document.query_selector("body").unwrap().unwrap();
        let body_class_list = body.class_list();
        body_class_list.set_value("sidebar-mini");

        if self.app_state.creds.project_names.is_empty()
            || self.app_state.creds.group_name.is_empty()
        {
            return html! {<start::StartModel on_start_callback=&self.link.callback(Msg::StartClicked) />};
        } else {
            return html! {<home::HomeModel app_state=&*self.app_state on_app_state_updated=self.link.callback(|new_app_state| Msg::AppStateUpdated(new_app_state)) />};
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        true
    }
}
