#![recursion_limit = "4096"]

#[macro_use]
extern crate dotenv_codegen;

#[macro_use]
extern crate log;

#[macro_use]
extern crate cfg_if;

mod app;
mod components;
mod event_bus;
mod home;
mod start;
mod types;
mod utils;

// extern crate serde;
// extern crate web_logger;
// extern crate web_sys;
// extern crate yew;
// extern crate yew_router;
extern crate wasm_bindgen;

use app::App;
use wasm_bindgen::prelude::*;

cfg_if! {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function to get better error messages if we ever panic.
    if #[cfg(feature = "console_error_panic_hook")] {
        extern crate console_error_panic_hook;
        use console_error_panic_hook::set_once as set_panic_hook;
    } else {
        #[inline]
        fn set_panic_hook() {}
    }
}

cfg_if! {
    // When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
    // allocator.
    if #[cfg(feature = "wee_alloc")] {
        extern crate wee_alloc;
        #[global_allocator]
        static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;
    }
}

// Called by our JS entry point to run the example
#[wasm_bindgen(start)]
pub fn main_js() {
    // If the `console_error_panic_hook` feature is enabled this will set a panic hook, otherwise
    // it will do nothing.
    set_panic_hook();
    web_logger::init();

    info!("starting up");

    yew::start_app::<App>();
}
