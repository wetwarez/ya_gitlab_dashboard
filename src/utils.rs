use crate::types::general::Creds;

pub const API_URL_PREFIX: &str = "https://gitlab.com/api/v4/";

pub fn get_cred_settings() -> Creds {
    let api_key: String = String::from(dotenv!("API_KEY"));
    let project_names: String = String::from(dotenv!("PROJECT_NAMES"));
    let group_name: String = String::from(dotenv!("GROUP_NAME"));
    let projects_requested = project_names.split(',').map(|s| s.to_string()).collect();

    Creds {
        api_key,
        project_names,
        group_name,
        projects_requested,
    }
}
