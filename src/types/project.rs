use crate::types::general::{
    Commit, CommitsListResult, Pipeline, PipelinesListResult, ProjectsListResult,
};
use std::collections::HashMap;
// use crate::types::project::Project;
use anyhow::Error;
use http::header::HeaderValue;
use std::collections::HashSet;
use std::iter::FromIterator;
use yew::callback::Callback;
use yew::format::{Json, Nothing};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

use crate::utils::API_URL_PREFIX;

#[derive(Default, Debug, Clone, PartialEq, serde_derive::Serialize, serde_derive::Deserialize)]
pub struct Project {
    pub id: u32,
    pub description: Option<String>,
    pub name: String,
    pub name_with_namespace: String,
    pub path: String,
    pub path_with_namespace: String,
    pub created_at: String,
    pub default_branch: Option<String>,

    // #[serde(default)]
    // pub branches: Vec<Branch>,
    #[serde(default)]
    pub pipelines: HashMap<i64, Pipeline>,

    // Not in the API
    #[serde(default)]
    pub commits: HashMap<String, Commit>,
    // pub tag_list: Vec<String>,
    // pub ssh_url_to_repo: String,
    // pub http_url_to_repo: String,
    // pub web_url: String,
    // pub readme_url: Option<String>,
    // pub avatar_url: Option<String>,
    // pub star_count: i64,
    // pub forks_count: i64,
    // pub last_activity_at: String,
    // pub namespace: Namespace,
    // #[serde(rename = "_links")]
    // pub links: Links,
    // pub empty_repo: bool,
    // pub archived: bool,
    // pub visibility: String,
    // pub resolve_outdated_diff_discussions: bool,
    // pub container_registry_enabled: bool,
    // pub container_expiration_policy: Option<ContainerExpirationPolicy>,
    // pub issues_enabled: bool,
    // pub merge_requests_enabled: Option<bool>,
    // pub wiki_enabled: bool,
    // pub jobs_enabled: bool,
    // pub snippets_enabled: bool,
    // pub can_create_merge_request_in: bool,
    // pub issues_access_level: String,
    // pub repository_access_level: String,
    // pub merge_requests_access_level: String,
    // pub wiki_access_level: String,
    // pub builds_access_level: String,
    // pub snippets_access_level: String,
    // pub pages_access_level: String,
    // pub emails_disabled: Option<bool>,
    // pub shared_runners_enabled: bool,
    // pub lfs_enabled: bool,
    // pub creator_id: i64,
    // pub import_status: String,
    // pub open_issues_count: Option<i64>,
    // pub ci_default_git_depth: Option<i64>,
    // pub public_jobs: bool,
    // pub build_timeout: i64,
    // pub auto_cancel_pending_pipelines: String,
    // pub build_coverage_regex: Option<String>,
    // pub ci_config_path: Option<String>,
    // pub shared_with_groups: Vec<SharedWithGroup>,
    // pub only_allow_merge_if_pipeline_succeeds: bool,
    // pub request_access_enabled: bool,
    // pub only_allow_merge_if_all_discussions_are_resolved: bool,
    // pub remove_source_branch_after_merge: Option<bool>,
    // pub printing_merge_request_link_enabled: bool,
    // pub merge_method: String,
    // pub suggestion_commit_message: Option<String>,
    // pub auto_devops_enabled: bool,
    // pub auto_devops_deploy_strategy: String,
    // pub autoclose_referenced_issues: bool,
    // pub approvals_before_merge: i64,
    // pub mirror: bool,
    // pub external_authorization_classification_label: String,
    // pub packages_enabled: bool,
    // pub service_desk_enabled: bool,
    // pub service_desk_address: Option<String>,
    // pub marked_for_deletion_at: Option<String>,
    // pub mirror_user_id: Option<i64>,
    // pub mirror_trigger_builds: Option<bool>,
    // pub only_mirror_protected_branches: Option<bool>,
    // pub mirror_overwrites_diverged_branches: Option<bool>,
    // pub forked_from_project: Option<ForkedFromProject>,
}

impl Project {
    fn sorted_pipelines(&self) -> Vec<Pipeline> {
        let mut pipelines: Vec<Pipeline> = self.pipelines.values().cloned().collect();
        pipelines.sort_by(|a, b| b.updated_at.cmp(&a.updated_at));
        pipelines
    }

    pub fn get_branch_names(&self) -> Vec<String> {
        // Put the default branch at the top
        let pipelines = self.sorted_pipelines();
        let mut branch_names_set = HashSet::new();
        for pipeline in pipelines {
            branch_names_set.insert(pipeline.ref_field.clone());
        }
        let branch_names: Vec<String> = match self.default_branch.clone() {
            Some(default_branch) => {
                let mut branches = vec![default_branch.clone()];
                let mut non_default_branches = Vec::from_iter(branch_names_set);
                non_default_branches.retain(|x| x != &default_branch);
                branches.extend(non_default_branches);
                branches
            }
            None => Vec::from_iter(branch_names_set),
        };
        branch_names
    }

    pub fn get_latest_pipeline_per_branch(&self, branch_name: &String) -> Option<Pipeline> {
        let mut pipelines = self.get_pipelines_per_branch(branch_name);
        pipelines.sort_by(|a, b| b.id.cmp(&a.id));

        if pipelines.len() == 0 {
            None
        } else {
            Some(pipelines[0].clone())
        }
    }

    pub fn get_pipelines_per_branch(&self, branch_name: &String) -> Vec<Pipeline> {
        let mut pipelines = self.sorted_pipelines();
        pipelines.retain(|pipeline| pipeline.ref_field == *branch_name);
        pipelines
    }

    pub fn get_latest_pipeline_branch_with_status(
        &self,
        status: &str,
        branch_name: &String,
    ) -> Option<Pipeline> {
        let pipelines = self.get_pipelines_per_branch(branch_name);
        for pipeline in pipelines {
            if pipeline.status == *status {
                return Some(pipeline.clone());
            }
        }
        return None;
    }

    pub fn pipelines_fetch_task(
        &self,
        page_num: u16,
        callback: Callback<PipelinesListResult>,
        api_key: String,
    ) -> FetchTask {
        let mut web = FetchService::new();
        let url = format!(
            "{}projects/{}/pipelines?page={}",
            API_URL_PREFIX, self.id, page_num
        );

        let project_id = self.id.clone();

        let handler = move |response: Response<Json<Result<Vec<Pipeline>, Error>>>| {
            let (meta, Json(data)) = response.into_parts();
            let page_num: String = String::from(
                meta.headers
                    .get("X-Page")
                    .unwrap_or(&HeaderValue::from_static(""))
                    .to_str()
                    .unwrap(),
            );
            let result = PipelinesListResult {
                pipelines_result: data,
                meta,
                project_id,
                page_num,
            };
            callback.emit(result);
        };

        let mut request = Request::builder();
        request = request
            .uri(url.as_str())
            .header("Accept", "application/json");
        if !api_key.is_empty() {
            request = request.header("PRIVATE-TOKEN", api_key);
        }
        web.fetch(request.body(Nothing).unwrap(), handler.into())
            .unwrap()
    }

    pub fn commits_fetch_task(
        &self,
        page_num: u16,
        callback: Callback<CommitsListResult>,
        api_key: String,
    ) -> FetchTask {
        let mut web = FetchService::new();
        let project_id = self.id.clone();
        let url = format!(
            "{}projects/{}/repository/commits?page={}",
            API_URL_PREFIX, project_id, page_num
        );

        let handler = move |response: Response<Json<Result<Vec<Commit>, Error>>>| {
            let (meta, Json(data)) = response.into_parts();
            let page_num: String = String::from(
                meta.headers
                    .get("X-Page")
                    .unwrap_or(&HeaderValue::from_static(""))
                    .to_str()
                    .unwrap(),
            );
            callback.emit(CommitsListResult {
                commits_result: data,
                meta,
                project_id,
                page_num,
            })
        };

        let mut request = Request::builder();
        request = request
            .uri(url.as_str())
            .header("Accept", "application/json");
        if !api_key.is_empty() {
            request = request.header("PRIVATE-TOKEN", api_key);
        }
        web.fetch(request.body(Nothing).unwrap(), handler.into())
            .unwrap()
    }
}

pub fn fetch_all_projects_task(
    group_path: String,
    page_num: u16,
    callback: Callback<ProjectsListResult>,
    api_key: String,
) -> FetchTask {
    let mut web = FetchService::new();

    let url = format!(
        "{}groups/{}/projects?page={}",
        API_URL_PREFIX, group_path, page_num
    );

    let handler = move |response: Response<Json<Result<Vec<Project>, Error>>>| {
        let (meta, Json(data)) = response.into_parts();
        let page_num: String = String::from(
            meta.headers
                .get("X-Page")
                .unwrap_or(&HeaderValue::from_static(""))
                .to_str()
                .unwrap(),
        );
        callback.emit(ProjectsListResult {
            projects_result: data,
            meta,
            page_num,
        })
    };

    let mut request = Request::builder();
    request = request
        .uri(url.as_str())
        .header("Accept", "application/json");
    if !api_key.is_empty() {
        request = request.header("PRIVATE-TOKEN", api_key);
    }
    web.fetch(request.body(Nothing).unwrap(), handler.into())
        .unwrap()
}
