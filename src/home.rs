use crate::components::footer::Footer;
use crate::components::header::Header;
use crate::components::info::InfoModel;
use crate::components::left_menu::LeftMenu;
use crate::components::right_menu::RightMenu;
use crate::types::app_state::AppState;
use crate::types::general::{ProjectsListResult, ViewSelected};
use crate::types::group::{group_fetch_task, Group};
use crate::types::project::{fetch_all_projects_task, Project};
use anyhow::Error;
use yew::prelude::*;
use yew::services::fetch::FetchTask;
use yew::services::Task;

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub app_state: AppState,
    pub on_app_state_updated: Callback<AppState>,
}

pub enum Msg {
    FetchGroup,
    GroupFetchReady(Result<Group, Error>),
    FetchProjectsPage(u16),
    ProjectsFetchReady(ProjectsListResult),
    AllProjectsRetrieved,
    ViewChanged(ViewSelected),
    AppStateUpdated(AppState),
}

#[derive(Debug)]
pub struct HomeModel {
    props: Props,
    link: ComponentLink<HomeModel>,
    fetch_tasks: Vec<Option<FetchTask>>,
    group_callback: Callback<Result<Group, Error>>,
    projects_callback: Callback<ProjectsListResult>,
    // project_branches_callback: Callback<BranchesListResult>,
    fetch_all_projects_filter: bool,
    current_view: ViewSelected,
}

impl Component for HomeModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let fetch_all_projects_filter = !props.app_state.creds.projects_requested.is_empty();
        let model = HomeModel {
            link: link.clone(),
            fetch_tasks: Vec::new(),
            props,
            group_callback: link.callback(Msg::GroupFetchReady),
            projects_callback: link.callback(Msg::ProjectsFetchReady),
            fetch_all_projects_filter,
            current_view: ViewSelected::Default,
        };
        model.link.send_message(Msg::FetchGroup);
        model
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::FetchGroup => {
                let group_name = self.props.app_state.creds.group_name.clone();
                let api_key = self.props.app_state.creds.api_key.clone();
                // Gather group info if it's not there
                if self.props.app_state.group.id == 0 as i64 {
                    let fetch_task =
                        group_fetch_task(group_name, self.group_callback.clone(), api_key);
                    self.fetch_tasks.push(Some(fetch_task));
                }
                true
            }

            Msg::GroupFetchReady(fetch_result) => match fetch_result {
                Ok(group) => {
                    self.props.app_state.group = group;
                    self.props.app_state.save_state();
                    self.link.send_message(Msg::FetchProjectsPage(1));
                    true
                }
                Err(err) => {
                    info!("GroupFetchReady Error {:?}", err);
                    true
                }
            },

            Msg::FetchProjectsPage(page_num) => {
                let group_path = self.props.app_state.group.path.clone();
                let api_key = self.props.app_state.creds.api_key.clone();

                let fetch_task = fetch_all_projects_task(
                    group_path,
                    page_num,
                    self.projects_callback.clone(),
                    api_key,
                );
                self.fetch_tasks.push(Some(fetch_task));
                true
            }

            Msg::ProjectsFetchReady(projects_list_result) => {
                // Request failed, return
                if !projects_list_result.meta.status.is_success() {
                    info!(
                        "ProjectsFetchReady Failed, {:?}",
                        projects_list_result.meta.status
                    );
                    return false;
                }

                let mut should_render = false;

                match projects_list_result.projects_result {
                    Ok(projects_list) => {
                        // Just get the projects on page one and return
                        if !self.fetch_all_projects_filter {
                            for project in projects_list.iter() {
                                self.props
                                    .app_state
                                    .projects
                                    .insert(project.id, project.clone());
                            }
                            self.props.app_state.save_state();
                            self.link.send_message(Msg::AllProjectsRetrieved);
                            return true;
                        }

                        let filtered_projects: Vec<&Project> = projects_list
                            .iter()
                            .filter(|project| {
                                self.props
                                    .app_state
                                    .creds
                                    .projects_requested
                                    .contains(&project.path)
                            })
                            .collect();

                        for &project in filtered_projects.iter() {
                            // self.props.app_state.projects.push(project.clone());
                            self.props
                                .app_state
                                .projects
                                .insert(project.id, project.clone());
                            should_render = true;
                        }
                        self.props.app_state.save_state();

                        let projects_requested_len =
                            self.props.app_state.creds.projects_requested.len();
                        let projects_retrieved_len = self.props.app_state.projects.len();

                        // Nothing left to do
                        if projects_requested_len == projects_retrieved_len {
                            self.link.send_message(Msg::AllProjectsRetrieved);
                            return should_render;
                        }

                        if let Some(current_page_num) =
                            projects_list_result.meta.headers.get("x-page")
                        {
                            if let Some(total_pages) =
                                projects_list_result.meta.headers.get("x-total-pages")
                            {
                                let current_page_num =
                                    current_page_num.to_str().unwrap().parse::<u16>().unwrap();
                                let total_pages =
                                    total_pages.to_str().unwrap().parse::<u16>().unwrap();

                                if current_page_num == total_pages {
                                    // This is the last page, nothing left to do
                                    self.link.send_message(Msg::AllProjectsRetrieved);
                                } else {
                                    // get next page
                                    self.link
                                        .send_message(Msg::FetchProjectsPage(current_page_num + 1));
                                }
                            }
                        }
                    }
                    Err(e) => {
                        info!("ProjectsFetchReady Could not get projects, {:?}", e);
                    }
                }
                should_render
            }

            Msg::AllProjectsRetrieved => true,

            Msg::ViewChanged(view_selected) => {
                self.current_view = view_selected;
                true
            }

            Msg::AppStateUpdated(new_app_state) => {
                self.props.app_state = new_app_state;
                self.props.on_app_state_updated.emit(AppState::default());
                false
            }
        }
    }

    fn rendered(&mut self, _: bool) {
        self.remove_completed_tasks();
    }

    fn view(&self) -> Html {
        html! {
            <div class="wrapper">
                <Header app_state=&self.props.app_state on_view_selected=self.link.callback(|view_selected| Msg::ViewChanged(view_selected)) on_app_state_updated=self.link.callback(|new_app_state| Msg::AppStateUpdated(new_app_state)) />

                <LeftMenu app_state=&self.props.app_state on_view_selected=self.link.callback(|view_selected| Msg::ViewChanged(view_selected)) />

                <InfoModel app_state=&self.props.app_state selected_view=&self.current_view on_app_state_updated=self.link.callback(|new_app_state| Msg::AppStateUpdated(new_app_state)) on_view_selected=self.link.callback(|view_selected| Msg::ViewChanged(view_selected))  />

                <Footer />

                <RightMenu />
            </div>
        }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        self.props = new_props;
        true
    }
}

impl HomeModel {
    fn remove_completed_tasks(&mut self) {
        self.fetch_tasks.retain(|t| t.as_ref().is_some());
        self.fetch_tasks.retain(|t| t.as_ref().unwrap().is_active());
    }
}
