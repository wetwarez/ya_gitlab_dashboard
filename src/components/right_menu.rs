use yew::prelude::*;
use yew::{html, Component, ComponentLink, Html, ShouldRender};

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {}

pub struct RightMenu {
    pub link: ComponentLink<RightMenu>,
    pub props: Props,
}

pub enum Msg {}

impl Component for RightMenu {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        RightMenu { props, link }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html! {
            <aside class="control-sidebar control-sidebar-dark">
                <div class="p-3">
                    <h5>{"Title"}</h5>
                    <p>{"Sidebar content"}</p>
                </div>
            </aside>
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
