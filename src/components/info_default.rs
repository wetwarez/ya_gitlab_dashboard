use crate::types::project::Project;
use std::collections::BTreeMap;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub projects: BTreeMap<u32, Project>,
}

pub enum Msg {}

pub struct InfoDefaultModel {
    props: Props,
}

impl Component for InfoDefaultModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        InfoDefaultModel { props }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {


        // <div class="content-wrapper" style="min-height: 1200.88px;">
        <>
          <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>{"Overview"}</h1>
                </div>
              </div>
            </div>
          </section>

          <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">{"Default branch status per project"}</h3>
                    </div>
                    <div class="card-body p-0">
                      <table id="data_table" class="table">
                        <thead>
                          <tr>
                            <th>{"Project"}</th>
                            <th>{"Path"}</th>
                            <th>{"Default Branch"}</th>
                            <th>{"Status"}</th>
                            <th>{"Last success"}</th>
                            <th>{"Last failure"}</th>
                            <th>{"sha"}</th>
                        </tr>
                        </thead>
                        <tbody>
                            { self.props.projects.values().map(|project| self.view_projects_summary(project.clone())).collect::<Html>() }
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </>
        // </div>


                      }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        self.props = new_props;
        true
    }
}

impl InfoDefaultModel {
    fn view_projects_summary(&self, project: Project) -> Html {
        if project.default_branch.is_none() {
            return html! {
                <tr>
                    <td>{ project.name.clone() }</td>
                    <td>{ project.path.clone() }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                </tr>
            };
        }

        let default_branch = project.default_branch.clone().unwrap();
        let latest_default_pipeline = project.get_latest_pipeline_per_branch(&default_branch);

        if latest_default_pipeline.is_none() {
            return html! {
                <tr>
                    <td>{ project.name.clone() }</td>
                    <td>{ project.path.clone() }</td>
                    <td>{ default_branch.clone() }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                    <td>{ "NA" }</td>
                </tr>
            };
        }

        let latest_status = latest_default_pipeline.clone().unwrap().status;
        let mut latest_sha = latest_default_pipeline.clone().unwrap().sha;
        latest_sha.truncate(8);

        let last_pass: String =
            match project.get_latest_pipeline_branch_with_status("success", &default_branch) {
                Some(pipeline) => pipeline.updated_at.clone(),
                None => String::from("N/A"),
            };

        let last_fail: String =
            match project.get_latest_pipeline_branch_with_status("failed", &default_branch) {
                Some(pipeline) => pipeline.updated_at.clone(),
                None => String::from("N/A"),
            };

        let show_status_span = |status| -> Html {
            match status {
                "success" => return html! { <span class="badge bg-success">{"Success"}</span> },

                "failed" => return html! { <span class="badge bg-danger">{"Failed"}</span> },

                _ => return html! { <span class="badge bg-warning">{"N/A"}</span> },
            }
        };

        html! {
            <tr>
                <td>{ project.name.clone() }</td>
                <td><a class="link" href=format!("https://gitlab.com/{}", project.path_with_namespace.clone()) target={ "_blank" }><i class="pl-1 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ project.clone().path }</span></a></td>
                <td>{ default_branch.clone() }</td>
                <td>{ show_status_span(&latest_status.as_str()) }</td>
                <td>{ last_pass }</td>
                <td>{ last_fail }</td>
                <td><a class="link" href={  latest_default_pipeline.unwrap().web_url.clone() }><i class="pl-1 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ latest_sha }</span></a></td>
            </tr>
        }
    }
}
