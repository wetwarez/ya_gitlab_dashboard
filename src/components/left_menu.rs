use super::group::GroupModel;
use super::projects_list::ProjectsList;
use crate::types::app_state::AppState;
use crate::types::general::ViewSelected;
use yew::prelude::*;
use yew::{html, Component, ComponentLink, Html, ShouldRender};

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub app_state: AppState,
    pub on_view_selected: Callback<ViewSelected>,
}

pub struct LeftMenu {
    pub link: ComponentLink<LeftMenu>,
    pub props: Props,
}

pub enum Msg {
    ViewChanged(ViewSelected),
}

impl Component for LeftMenu {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        LeftMenu { props, link }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ViewChanged(view_selected) => {
                self.props.on_view_selected.emit(view_selected);
                false
            }
        }
    }

    fn view(&self) -> Html {
        html! {

            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <a href="#" class="brand-link">
                    <i class="pl-3 fab fa-gitlab"></i>
                    <span class="pl-2 brand-text font-weight-light">{"YAGD"}</span>
                </a>
                <div class="sidebar">
                    <GroupModel on_view_selected=self.link.callback(|view_selected| Msg::ViewChanged(view_selected)) group=&self.props.app_state.group />
                    <ProjectsList app_state=&self.props.app_state on_view_selected=self.link.callback(|view_selected| Msg::ViewChanged(view_selected)) />
                </div>
            </aside>
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
