use crate::types::app_state::AppState;
use crate::types::general::ViewSelected;
use crate::types::project::Project;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone)]
pub struct Props {
    pub app_state: AppState,
    pub on_view_selected: Callback<ViewSelected>,
}

pub enum Msg {
    ClickedProject(u32),
}

pub struct ProjectsList {
    props: Props,
    link: ComponentLink<ProjectsList>,
}

impl Component for ProjectsList {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ProjectsList {
            props,
            link: link.clone(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        info!("ProjectsList update");

        match msg {
            Msg::ClickedProject(project_id) => {
                info!("ClickedProject {:?}", project_id);
                if let Some(project) = self.props.app_state.projects.get(&project_id) {
                    self.props
                        .on_view_selected
                        .emit(ViewSelected::Project(project.clone()));
                }
                false
            }
        }
    }

    fn view(&self) -> Html {
        if self.props.app_state.projects.is_empty() {
            self.view_fetcing()
        } else {
            self.view_list()
        }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        self.props = new_props;
        true
    }
}

impl ProjectsList {
    fn view_fetcing(&self) -> Html {
        html! {
            <>
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="pure-menu-item">
                            <a href="#" class="pure-menu-link">{"Fetching ..."}</a>
                        </li>
                    </ul>
                </nav>
            </>
        }
    }

    fn view_list(&self) -> Html {
        html! {
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    {self.props.app_state.projects.values().map(|project| self.view_project(project.clone())).collect::<Html>()}
                </ul>
            </nav>
        }
    }

    fn view_project(&self, project: Project) -> Html {
        let project_id = project.id.clone();
        html! {
                <>
                    <li class="nav-item">
                        <a onclick=self.link.callback(move |_| Msg::ClickedProject(project_id)) href="#" class="nav-link">
                            <i class="nav-icon fas fa-th-list" style="white-space: normal;"></i>
                            <p>
                                { project.name.clone() }
                            </p>
                        </a>
                    </li>
                </>
        }
    }
}
