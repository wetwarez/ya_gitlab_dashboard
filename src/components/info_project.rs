use crate::types::general::ViewSelected;
use crate::types::project::Project;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub project: Project,
    pub on_view_selected: Callback<ViewSelected>,
    pub on_update_project: Callback<u32>,
}

pub enum Msg {
    ClickedBranch(String, Project),
    ClickedProjectUpdate,
}

pub struct InfoProjectModel {
    props: Props,
    link: ComponentLink<InfoProjectModel>,
}

impl Component for InfoProjectModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        InfoProjectModel { props, link }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ClickedBranch(branch_ref, project) => {
                self.props
                    .on_view_selected
                    .emit(ViewSelected::Branch(project, branch_ref));
                false
            }

            Msg::ClickedProjectUpdate => {
                self.props.on_update_project.emit(self.props.project.id);
                false
            }
        }
    }

    fn view(&self) -> Html {
        let branch_names = self.props.project.get_branch_names();

        html! {
            <>
            <section class="content-header">
                <div class="container-fluid">

                <div class="row">
                    <div class="col-11">
                      <h1>{ self.props.project.name.clone() }</h1>
                    </div>
                    <div class="col-1">
                        <a href="#" data-toggle="dropdown" class="nav-link">
                            <i style="color:green;" class="fas fa-cloud-download-alt fa-lg"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                            <a onclick=self.link.callback(|_| Msg::ClickedProjectUpdate) class="dropdown-item" href="#">
                                <i class="fas fa-cloud-download-alt mr-2 fa-lg" style="color:green;"></i>{ "Refresh Project Data"}
                            </a>
                        <div class="dropdown-divider">
                    </div>
                    </div>
                    </div>
                  </div>
                </div>
            </section>

            <section class="content">
                <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        <h3 class="card-title">{"Branch summaries"}</h3>
                        </div>
                        <div class="card-body p-0">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{""}</th>
                                    <th>{"Branch"}</th>
                                    <th>{"Status"}</th>
                                    <th>{"Last success"}</th>
                                    <th>{"Last failure"}</th>
                                    <th>{"sha"}</th>
                                    <th>{"Commits"}</th>
                                </tr>
                            </thead>
                            <tbody>

                                { branch_names.iter().map(|(branch_name)| self.view_branch(branch_name.clone())).collect::<Html>() }

                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </section>
            </>
        }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        self.props = new_props;
        true
    }
}

impl InfoProjectModel {
    fn view_branch(&self, branch_name: String) -> Html {
        let show_status_span = |status| -> Html {
            match status {
                "success" => return html! { <span class="badge bg-success">{"Success"}</span> },

                "failed" => return html! { <span class="badge bg-danger">{"Failed"}</span> },

                _ => return html! { <span class="badge bg-warning">{"N/A"}</span> },
            }
        };

        match self
            .props
            .project
            .get_latest_pipeline_per_branch(&branch_name)
        {
            Some(latest_pipeline) => {
                let last_pass: String = match self
                    .props
                    .project
                    .get_latest_pipeline_branch_with_status("success", &branch_name)
                {
                    Some(last_pass_pipeline) => last_pass_pipeline.updated_at.clone(),
                    None => String::from("N/A"),
                };

                let last_fail: String = match self
                    .props
                    .project
                    .get_latest_pipeline_branch_with_status("failed", &branch_name)
                {
                    Some(last_fail_pipeline) => last_fail_pipeline.updated_at.clone(),
                    None => String::from("N/A"),
                };

                let button_branch = branch_name.clone();
                let button_project = self.props.project.clone();
                let mut display_sha = latest_pipeline.sha.clone();
                display_sha.truncate(8);

                return html! {
                    <tr>
                        <td>
                            <button onclick=self.link.callback(move |_| Msg::ClickedBranch(button_branch.clone(), button_project.clone())) type="button" class="btn btn-primary px-3 btn-sm">
                                <i class="nav-icon fas fa-code-branch" aria-hidden="true"></i>
                            </button>
                        </td>
                        <td>{ branch_name.clone() }</td>
                        <td>{ show_status_span( latest_pipeline.status.as_str() ) }</td>
                        <td>{ last_pass }</td>
                        <td>{ last_fail }</td>
                        <td><a class="link" href={ latest_pipeline.web_url.clone() }><i class="pl-1 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{ display_sha }</span></a></td>
                        <td><a class="link" href={ format!("https://gitlab.com/{}/-/commits/{}", self.props.project.name_with_namespace.clone(), latest_pipeline.ref_field.clone()) } target={ "_blank" }><i class="pl-1 fab fa-gitlab"></i><span class="pl-2 brand-text font-weight-light">{  "commits"  }</span></a></td>
                    </tr>
                };
            }
            None => {
                return html! {
                    <tr>
                        <td>{""}</td>
                        <td>{ branch_name.clone() }</td>
                        <td>{"N/A"}</td>
                        <td>{"N/A"}</td>
                        <td>{"N/A"}</td>
                        <td>{"N/A"}</td>
                        <td>{"N/A"}</td>
                    </tr>
                };
            }
        }
    }
}
