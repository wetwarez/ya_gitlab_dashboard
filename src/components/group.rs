use crate::types::general::ViewSelected;
use crate::types::group::Group;
use yew::callback::Callback;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub group: Group,
    pub on_view_selected: Callback<ViewSelected>,
}

pub enum Msg {
    ClickedHome,
}

pub struct GroupModel {
    props: Props,
    link: ComponentLink<GroupModel>,
}

impl Component for GroupModel {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        GroupModel { link, props }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ClickedHome => {
                self.props.on_view_selected.emit(ViewSelected::Default);
                false
            }
        }
    }

    fn view(&self) -> Html {
        html! {
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <a onclick=self.link.callback(|_| Msg::ClickedHome) class="pl-2 d-block">{ self.props.group.name.as_str() }</a>
                </div>
            </div>
        }
    }

    fn change(&mut self, new_props: Self::Properties) -> ShouldRender {
        if self.props.group.name == new_props.group.name
            && self.props.group.description == new_props.group.description
        {
            false
        } else {
            self.props = new_props;
            true
        }
    }
}
