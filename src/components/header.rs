use crate::types::app_state::AppState;
use crate::types::general::ViewSelected;
use crate::wasm_bindgen;
use yew::prelude::*;
use yew::{html, Component, ComponentLink, Html, ShouldRender};

#[wasm_bindgen(module = "/js/utils.js")]
extern "C" {
    fn confirm_delete() -> bool;
}

#[derive(PartialEq, Properties, Clone, Debug)]
pub struct Props {
    pub on_view_selected: Callback<ViewSelected>,
    pub app_state: AppState,
    pub on_app_state_updated: Callback<AppState>,
}

pub struct Header {
    pub link: ComponentLink<Header>,
    pub props: Props,
}

pub enum Msg {
    ClickedHome,
    ClickedRestart,
}

impl Component for Header {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Header { props, link }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ClickedHome => {
                self.props.on_view_selected.emit(ViewSelected::Default);
                false
            }

            Msg::ClickedRestart => match confirm_delete() {
                true => {
                    self.props.app_state.delete_state();
                    self.props.on_app_state_updated.emit(AppState::default());
                    true
                }
                false => false,
            },
        }
    }

    fn view(&self) -> Html {
        html! {
            <>
                <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                            <a onclick=self.link.callback(|_| Msg::ClickedHome) href="#" class="nav-link">{"Home"}</a>
                        </li>

                    </ul>

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">

                            <a class="nav-link" data-toggle="dropdown" href="#">
                            <i class="fas fa-cloud-download-alt fa-lg" style="color:red;"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                            <a onclick=&self.link.callback(|_| Msg::ClickedRestart) href="#" class="dropdown-item">
                                <i class="fas fa-trash mr-2 fa-lg" style="color:red;"></i>{ format!("Delete all data and start over") }
                            </a>
                            <div class="dropdown-divider"></div>

                            </div>
                        </li>
                    </ul>

                </nav>

            </>
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
