import * as AdminLte from "admin-lte";

export function re_init_tree_view() {
  $('[data-widget="treeview"]').each(function () {
    AdminLte.Treeview._jQueryInterface.call($(this), "init");
  });
}

export function confirm_delete() {
  return confirm(
    "Are you sure you want to delete all data and return to the start screen ?"
  );
}
